﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="viewSearchMatricula.aspx.cs" Inherits="ColegioClienteNET_FireBase.Views.viewSearchMatricula" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Cliente .NET FireBase | Colegio</title>
    <link rel="stylesheet" href="../Libs/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../libs/css/footer.css" />
    <link rel="stylesheet" href="../libs/css/styles.css" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <a class="navbar-brand" href="#">Colegio</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="">Principal</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Matricula</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="viewListMatricula.aspx">Listar | Matricula</a>
                        <a class="dropdown-item" href="viewAddMatricula.aspx">Agregar | Matricula</a>
                        <a class="dropdown-item" href="viewSearchMatricula.aspx">Filtrar Matricula por dos parametros</a>
                        <a class="dropdown-item" href="viewDeleteMatricula.aspx">Buscar, Eliminar | Matricula</a>
                        <a class="dropdown-item" href="viewEditMatricula.aspx">Buscar, Editar | Matricula</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="actualizar()">Actualizar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="acercaDe()">Acerca de ....</a>
                </li>
            </ul>
        </nav>
        <h2 class="title mb-5">Buscar Matricula</h2>

        <div class="cont-search">
            <div>

                <asp:TextBox ID="codigoEst" runat="server" CssClass="form-control" placeholder="Documento del estudiante"></asp:TextBox>
                <asp:Button ID="btnBuscarCodest" CssClass="btn btn-success" runat="server" Text="Buscar" OnClick="btnBuscarCodest_Click" />
            </div>

            <div>
                <select id="selectestado" class="form-control" runat="server">
                    <option value="none">Selecciona El estado</option>
                    <option value="0">Matriculada</option>
                    <option value="1">Cursando</option>
                    <option value="2">Rerpobado</option>
                    <option value="3">Aprobado</option>
                </select>


                <asp:Button ID="btnBuscarEstadoM" runat="server" CssClass="btn btn-success" Text="Buscar" OnClick="btnBuscarEstadoM_Click" />
            </div>
        </div>
        <br />
        <br />
        <div class="cont">
            <asp:GridView ID="grillaMatricula" runat="server"></asp:GridView>
        </div>


    </form>


    <script src="../libs/js/jquery-3.2.1.slim.min.js"></script>
    <script src="../libs/js/popper.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
    <script src="../libs/js/sweetalert.min.js"></script>
    <script src="../libs/js/fontawesome.js"></script>
    <script src="../libs/js/aplicacion.js"></script>
</body>
</html>

﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="viewEditMatricula.aspx.cs" Inherits="ColegioClienteNET_FireBase.Views.viewEditMatricula" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Cliente .NET FireBase | Colegio</title>
    <link rel="stylesheet" href="../Libs/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../libs/css/footer.css" />
    <link rel="stylesheet" href="../libs/css/styles.css" />
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <a class="navbar-brand" href="#">Colegio</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Principal</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Matricula</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="viewListMatricula.aspx">Listar | Matricula</a>
                        <a class="dropdown-item" href="viewAddMatricula.aspx">Agregar | Matricula</a>
                        <a class="dropdown-item" href="viewSearchMatricula.aspx">Filtrar Matricula por dos parametros</a>
                        <a class="dropdown-item" href="viewDeleteMatricula.aspx">Buscar, Eliminar | Matricula</a>
                        <a class="dropdown-item" href="viewEditMatricula.aspx">Buscar, Editar | Matricula</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="actualizar()">Actualizar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="acercaDe()">Acerca de ....</a>
                </li>
            </ul>
        </nav>

        <h2 class="title">Eliminar Matricula</h2>
        <asp:Panel ID="Panel1" runat="server" Visible="False">
            <div class="alert alert-success alert-dismissible fade show panel" role="alert">
                <strong>Correcto!</strong> Matricula editada.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
            </div>
        </asp:Panel>

        <div class="cont-search">
            <div>
                <asp:TextBox ID="txtdocest" runat="server" CssClass="form-control" placeholder="Documento del estudiante"></asp:TextBox>
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-success" OnClick="btnBuscar_Click" />
            </div>
            <div>
                <asp:TextBox ID="txtcodedit" runat="server" CssClass="form-control" placeholder="Codigo" Visible="False"></asp:TextBox>
                <asp:Button ID="btnedit" runat="server" CssClass="btn btn-primary" Text="Buscar para Editar" OnClick="btnedit_Click" Visible="False" />
            </div>
        </div>
        <br />
        <br />
        <div class="cont-update">
            <div class="group-conte">
                <asp:GridView ID="grillaListM" runat="server" CssClass="table table-bordered">
                </asp:GridView>
            </div>

            <div class="group-conte2">
                <asp:Panel ID="panelforms" runat="server" Visible="False">
                    <div class="form-group">
                        <label for="editpkest">Codigo Estudiante:</label>
                        <asp:TextBox ID="txtPkEst" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="editpkmat">Codigo Materia:</label>
                        <asp:TextBox ID="txtpkMate" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Fecha Inscripcion:</label>
                        <asp:TextBox ID="datefechains" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Fecha Inicio:</label>
                        <asp:TextBox ID="datefechin" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Fecha Final:</label>
                        <asp:TextBox ID="datefechfin" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="editpkest">Nota definitiva:</label>
                        <asp:TextBox ID="txtnotadef" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="editpkest">Estado de la Matricula:</label>
                        <select id="SelectGrado" class="form-control" runat="server">
                            <option value="0">Matriculado</option>
                            <option value="1">Cursando</option>
                            <option value="2">Reprobado</option>
                            <option value="3">Aprobado</option>
                        </select>
                    </div>
                    <asp:Button ID="btnactualizar" CssClass="btn btn-success" runat="server" Text="Actualizar" OnClick="btnactualizar_Click" />
                </asp:Panel>
            </div>


        </div>
        <br />
        <br />





    </form>


    <script src="../libs/js/jquery-3.2.1.slim.min.js"></script>
    <script src="../libs/js/popper.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
    <script src="../libs/js/sweetalert.min.js"></script>
    <script src="../libs/js/fontawesome.js"></script>
    <script src="../libs/js/aplicacion.js"></script>
</body>
</html>

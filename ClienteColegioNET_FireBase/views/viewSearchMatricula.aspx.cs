﻿using ClienteColegioNET_FireBase.servicio;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ColegioClienteNET_FireBase.Views
{
    public partial class viewSearchMatricula : System.Web.UI.Page
    {

        private FirestoreDb db = Connection.getConnection();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnBuscarCodest_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            DataRow dr;
            int doc = Convert.ToInt32(codigoEst.Text);

            dt.Columns.Add(new DataColumn("Codigo Matricula"));
            dt.Columns.Add(new DataColumn("Estudiante"));
            dt.Columns.Add(new DataColumn("Materia"));
            dt.Columns.Add(new DataColumn("Nota Definitiva"));
            dt.Columns.Add(new DataColumn("Fecha Inscripcion"));
            dt.Columns.Add(new DataColumn("Fecha Inicio"));
            dt.Columns.Add(new DataColumn("Fecha Final"));
            dt.Columns.Add(new DataColumn("Estado"));





            CollectionReference citiesRef = db.Collection("Matricula");
            Query query = citiesRef.WhereEqualTo("pkEstudiante", doc);
            QuerySnapshot querySnapshot = await query.GetSnapshotAsync();

            CollectionReference estudianteRef = db.Collection("Estudiante");
            Query queryest = estudianteRef.WhereEqualTo("documento", doc);
            QuerySnapshot queryestSnapshot = await queryest.GetSnapshotAsync();

            string nombreest = "";
            foreach (DocumentSnapshot docest in queryestSnapshot.Documents)
            {
                Dictionary<string, object> documentDictionaryest = docest.ToDictionary();

                nombreest = documentDictionaryest["nombres"] + " " + documentDictionaryest["apellidos"];
            }

            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
            {

                Dictionary<string, object> documentDictionary = documentSnapshot.ToDictionary();

                dr = dt.NewRow();

                int codmat = Convert.ToInt32(documentDictionary["pkMateria"].ToString());

                dr["Codigo Matricula"] = documentSnapshot.Id;
                dr["Estudiante"] = nombreest;


                CollectionReference materiasRef = db.Collection("Materia");
                Query querymate = materiasRef.WhereEqualTo("codigo", codmat);
                QuerySnapshot querymatSnapshot = await querymate.GetSnapshotAsync();

                foreach (DocumentSnapshot docmat in querymatSnapshot.Documents)
                {
                    Dictionary<string, object> documentDictionarymat = docmat.ToDictionary();
                    dr["Materia"] = documentDictionarymat["nombre"];
                }




                dr["Nota Definitiva"] = documentDictionary["notaDefinitiva"];
                dr["Fecha Inscripcion"] = documentDictionary["fechaInscripcion"];
                dr["Fecha Inicio"] = documentDictionary["fechaInicio"];
                dr["Fecha Final"] = documentDictionary["fechaFinal"];
                int estado = Convert.ToInt32(documentDictionary["estado"]);
                if (estado == 0)
                {
                    dr["Estado"] = "Matriculado";
                }
                else if (estado == 1)
                {
                    dr["Estado"] = "Cursando";
                }
                else if (estado == 2)
                {
                    dr["Estado"] = "Reprobado";
                }
                else if (estado == 3)
                {
                    dr["Estado"] = "Aprobado";
                }
                //dr["Eliminar"] = ;





                dt.Rows.Add(dr);

            }


            grillaMatricula.DataSource = dt;
            grillaMatricula.DataBind();
        }

        protected async void btnBuscarEstadoM_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            DataRow dr;
            int doc = Convert.ToInt32(selectestado.Value);

            dt.Columns.Add(new DataColumn("Codigo Matricula"));
            dt.Columns.Add(new DataColumn("Estudiante"));
            dt.Columns.Add(new DataColumn("Materia"));
            dt.Columns.Add(new DataColumn("Nota Definitiva"));
            dt.Columns.Add(new DataColumn("Fecha Inscripcion"));
            dt.Columns.Add(new DataColumn("Fecha Inicio"));
            dt.Columns.Add(new DataColumn("Fecha Final"));
            dt.Columns.Add(new DataColumn("Estado"));





            CollectionReference citiesRef = db.Collection("Matricula");
            Query query = citiesRef.WhereEqualTo("estado", doc);
            QuerySnapshot querySnapshot = await query.GetSnapshotAsync();

            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
            {

                Dictionary<string, object> documentDictionary = documentSnapshot.ToDictionary();

                dr = dt.NewRow();


                dr["Codigo Matricula"] = documentSnapshot.Id;
                int docu = Convert.ToInt32(documentDictionary["pkEstudiante"].ToString());
                int codmat = Convert.ToInt32(documentDictionary["pkMateria"].ToString());
                CollectionReference estudianteRef = db.Collection("Estudiante");
                Query queryest = estudianteRef.WhereEqualTo("documento", docu);
                QuerySnapshot queryestSnapshot = await queryest.GetSnapshotAsync();


                foreach (DocumentSnapshot docest in queryestSnapshot.Documents)
                {
                    Dictionary<string, object> documentDictionaryest = docest.ToDictionary();

                    dr["Estudiante"] = documentDictionaryest["nombres"] + " " + documentDictionaryest["apellidos"];
                }

                CollectionReference materiasRef = db.Collection("Materia");
                Query querymate = materiasRef.WhereEqualTo("codigo", codmat);
                QuerySnapshot querymatSnapshot = await querymate.GetSnapshotAsync();

                foreach (DocumentSnapshot docmat in querymatSnapshot.Documents)
                {
                    Dictionary<string, object> documentDictionarymat = docmat.ToDictionary();
                    dr["Materia"] = documentDictionarymat["nombre"];
                }

                dr["Nota Definitiva"] = documentDictionary["notaDefinitiva"];
                dr["Fecha Inscripcion"] = documentDictionary["fechaInscripcion"];
                dr["Fecha Inicio"] = documentDictionary["fechaInicio"];
                dr["Fecha Final"] = documentDictionary["fechaFinal"];
                int estado = Convert.ToInt32(documentDictionary["estado"]);
                if (estado == 0)
                {
                    dr["Estado"] = "Matriculado";
                }
                else if (estado == 1)
                {
                    dr["Estado"] = "Cursando";
                }
                else if (estado == 2)
                {
                    dr["Estado"] = "Reprobado";
                }
                else if (estado == 3)
                {
                    dr["Estado"] = "Aprobado";
                }
                //dr["Eliminar"] = ;





                dt.Rows.Add(dr);

            }


            grillaMatricula.DataSource = dt;
            grillaMatricula.DataBind();

        }
    }
}
﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="viewPrincipal.aspx.cs" Inherits="ColegioClienteNET_FireBase.Views.Principal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cliente .NET FireBase | Colegio</title>
    <link rel="stylesheet" href="../Libs/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../libs/css/footer.css"/>
    <link rel="stylesheet" href="../libs/css/styles.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <a class="navbar-brand" href="#">Colegio</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Principal</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Matricula</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="viewListMatricula.aspx">Listar | Matricula</a>
                        <a class="dropdown-item" href="viewAddMatricula.aspx">Agregar | Matricula</a>
                        <a class="dropdown-item" href="viewSearchMatricula.aspx">Filtrar Matricula por dos parametros</a>
                        <a class="dropdown-item" href="viewDeleteMatricula.aspx">Buscar, Eliminar | Matricula</a>
                        <a class="dropdown-item" href="viewEditMatricula.aspx">Buscar, Editar | Matricula</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="acercaDe()">Acerca de ....</a>
                </li>
            </ul>
        </nav>
    </form>




     <script src="../libs/js/jquery-3.2.1.slim.min.js"></script>
    <script src="../libs/js/popper.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
    <script src="../libs/js/sweetalert.min.js"></script>
    <script src="../libs/js/fontawesome.js"></script>
    <script src="../libs/js/aplicacion.js"></script>
</body>
</html>

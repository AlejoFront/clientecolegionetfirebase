﻿using ClienteColegioNET_FireBase.servicio;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ColegioClienteNET_FireBase.Views
{
    public partial class viewEditMatricula : System.Web.UI.Page
    {

        private FirestoreDb db = Connection.getConnection();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnBuscar_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            DataRow dr;

            int doc = Convert.ToInt32(txtdocest.Text);

            dt.Columns.Add(new DataColumn("Codigo Matricula"));
            dt.Columns.Add(new DataColumn("Estudiante"));
            dt.Columns.Add(new DataColumn("Materia"));
            dt.Columns.Add(new DataColumn("Nota Definitiva"));
            dt.Columns.Add(new DataColumn("Fecha Inicio"));
            dt.Columns.Add(new DataColumn("Fecha Final"));
            dt.Columns.Add(new DataColumn("Estado"));






            CollectionReference citiesRef = db.Collection("Matricula");
            Query query = citiesRef.WhereEqualTo("pkEstudiante", doc);
            QuerySnapshot querySnapshot = await query.GetSnapshotAsync();

            CollectionReference estudianteRef = db.Collection("Estudiante");
            Query queryest = estudianteRef.WhereEqualTo("documento", doc);
            QuerySnapshot queryestSnapshot = await queryest.GetSnapshotAsync();

            string nombreest = "";
            foreach (DocumentSnapshot docest in queryestSnapshot.Documents)
            {
                Dictionary<string, object> documentDictionaryest = docest.ToDictionary();

                nombreest = documentDictionaryest["nombres"] + " " + documentDictionaryest["apellidos"];
            }


            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
            {

                Dictionary<string, object> documentDictionary = documentSnapshot.ToDictionary();

                dr = dt.NewRow();


                dr["Codigo Matricula"] = documentSnapshot.Id;



                dr["Estudiante"] = nombreest;


                int codmat = Convert.ToInt32(documentDictionary["pkMateria"].ToString());

                

                CollectionReference materiasRef = db.Collection("Materia");
                Query querymate = materiasRef.WhereEqualTo("codigo", codmat);
                QuerySnapshot querymatSnapshot = await querymate.GetSnapshotAsync();

                foreach (DocumentSnapshot docmat in querymatSnapshot.Documents)
                {
                    Dictionary<string, object> documentDictionarymat = docmat.ToDictionary();
                    dr["Materia"] = documentDictionarymat["nombre"];
                }


                dr["Nota Definitiva"] = documentDictionary["notaDefinitiva"];
                dr["Fecha Inicio"] = documentDictionary["fechaInicio"];
                dr["Fecha Final"] = documentDictionary["fechaFinal"];
                int estado = Convert.ToInt32(documentDictionary["estado"]);
                if (estado == 0)
                {
                    dr["Estado"] = "Matriculado";
                }
                else if (estado == 1)
                {
                    dr["Estado"] = "Cursando";
                }
                else if (estado == 2)
                {
                    dr["Estado"] = "Reprobado";
                }
                else if (estado == 3)
                {
                    dr["Estado"] = "Aprobado";
                }

                dt.Rows.Add(dr);

            }


            grillaListM.DataSource = dt;
            grillaListM.DataBind();

            txtcodedit.Visible = true;
            btnedit.Visible = true;
            
        }

        protected async void btnedit_Click(object sender, EventArgs e)
        {
            panelforms.Visible = true;
            string codMatricula = txtcodedit.Text;

            DocumentReference docRef = db.Collection("Matricula").Document(codMatricula);
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();
            if (snapshot.Exists)
            {
                

                Dictionary<string, object> matricula = snapshot.ToDictionary();


                txtPkEst.Text = matricula["pkEstudiante"].ToString();
                txtpkMate.Text = matricula["pkMateria"].ToString();
                datefechains.Text = matricula["fechaInscripcion"].ToString();
                datefechin.Text = matricula["fechaInicio"].ToString();
                datefechfin.Text = matricula["fechaFinal"].ToString();
                txtnotadef.Text = matricula["notaDefinitiva"].ToString();
                SelectGrado.Value = matricula["estado"].ToString();

            }
            else
            {
                //Console.WriteLine("Document {0} does not exist!", snapshot.Id);
            }

            //txtPkEst.Text = ;
        }













        protected async void btnactualizar_Click(object sender, EventArgs e)
        {
            int codes = Convert.ToInt32(txtPkEst.Text);
            int codMat = Convert.ToInt32(txtpkMate.Text);
            string datini = datefechin.Text;
            string datfinal = datefechfin.Text;
            string datfeins = datefechains.Text;
            Double notfinal = Convert.ToDouble(txtnotadef.Text);
            int estatematri = Convert.ToInt32(SelectGrado.Value);
            string cod = txtcodedit.Text;
            DocumentReference matriculaRef = db.Collection("Matricula").Document(cod);
            Dictionary<string, object> updates = new Dictionary<string, object>
            {
                { "estado", estatematri },
                { "fechaFinal", datfinal },
                { "fechaInicio", datini },
                { "fechaInscripcion", datfeins },
                { "notaDefinitiva", notfinal },
                { "pkEstudiante", codes },
                { "pkMateria", codMat }
            };
            await matriculaRef.UpdateAsync(updates);

            Panel1.Visible = true;

            txtPkEst.Text = "";
            txtpkMate.Text = "";
            datefechin.Text = "";
            datefechfin.Text = "";
            datefechains.Text = "";
            txtnotadef.Text = "";
            txtcodedit.Text = "";



            panelforms.Visible = false;
        }
    }
}
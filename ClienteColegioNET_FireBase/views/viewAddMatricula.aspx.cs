﻿using ClienteColegioNET_FireBase.servicio;
using Google.Cloud.Firestore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ColegioClienteNET_FireBase.Views
{
    public partial class viewAddMatricula : System.Web.UI.Page
    {

        private FirestoreDb db = Connection.getConnection();
        int document; 
        private static ArrayList codigomaterias = new ArrayList();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected  async void btnBuscarEst_Click(object sender, EventArgs e)
        {

            System.Data.DataTable dt = new System.Data.DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("Nombre"));
            dt.Columns.Add(new DataColumn("Apellidos"));
            dt.Columns.Add(new DataColumn("Correo"));
            dt.Columns.Add(new DataColumn("Telefono"));

            document = Convert.ToInt32(txtDocumento.Text);


            CollectionReference citiesRef = db.Collection("Estudiante");
            Query query = citiesRef.WhereEqualTo("documento", document);
            QuerySnapshot querySnapshot = await query.GetSnapshotAsync();
            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
            {

                Dictionary<string, object> documentDictionary = documentSnapshot.ToDictionary();

                dr = dt.NewRow();


                dr["Nombre"] = documentDictionary["nombres"];
                dr["Apellidos"] = documentDictionary["apellidos"];
                dr["Correo"] = documentDictionary["correo"];
                dr["Telefono"] = documentDictionary["telefono"];

                dt.Rows.Add(dr);

            }


            grillalistest.DataSource = dt;
            grillalistest.DataBind();
        }

        protected async void btnBuscarGrado_Click(object sender, EventArgs e)
        {
            codigomaterias.Clear();
            System.Data.DataTable dt = new System.Data.DataTable();
            DataRow dr;
            dt.Columns.Add(new DataColumn("Codigo"));
            dt.Columns.Add(new DataColumn("Nombre Materia"));
            dt.Columns.Add(new DataColumn("Intensidad Horaria"));

            int idgrado = Convert.ToInt32(SelectGrado.Value);
            CollectionReference citiesRef = db.Collection("Materia");
            Query query = citiesRef.WhereEqualTo("grado", idgrado);
            QuerySnapshot querySnapshot = await query.GetSnapshotAsync();
            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
            {
                
                Dictionary<string, object> documentDictionary = documentSnapshot.ToDictionary();

                dr = dt.NewRow();


                dr["Codigo"] = documentDictionary["codigo"];
                dr["Nombre Materia"] = documentDictionary["nombre"];
                dr["Intensidad Horaria"] = documentDictionary["intensidad_horaria"];
               

                dt.Rows.Add(dr);

                
                codigomaterias.Add(Convert.ToInt32(documentDictionary["codigo"]));

            }

            

            grillalistgrado.DataSource = dt;
            grillalistgrado.DataBind();


            btnMatricular.Visible = true;
        }

        protected  void btnMatricular_Click(object sender, EventArgs e)
        {
            //String fechaActual = Convert.ToString(new DateTime());
            String fechaInicio = Convert.ToString(fechainicio.Value);
            String fechaFin = Convert.ToString(fechafin.Value);
            int document = Convert.ToInt32(txtDocumento.Text);
            //document,

            for (int i = 0; i < codigomaterias.Count; i++ )
            {


                CollectionReference docRef = db.Collection("Matricula");
                Dictionary<string, object> matricula = new Dictionary<string, object>
            {
                    { "estado", 0 },
                    { "fechaFinal", fechaFin },
                    { "fechaInicio", fechaInicio },
                    { "fechaInscripcion", fechaInicio },
                    { "notaDefinitiva", 0.0 },
                    { "pkEstudiante", document },
                    { "pkMateria", codigomaterias[i] }
            };
                docRef.AddAsync(matricula);




                //DocumentReference addedDocRef = await db.Collection("Matricula").AddAsync(matricula);
            }



        }
    }
}
﻿
using ClienteColegioNET_FireBase.servicio;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ColegioClienteNET_FireBase.Views
{
    public partial class viewListMatricula : System.Web.UI.Page
    {

        private  FirestoreDb db = Connection.getConnection();

        protected  async void Page_Load(object sender, EventArgs e)
        {


            DataTable dt = new System.Data.DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("Estudiante"));
            dt.Columns.Add(new DataColumn("Materia"));
            dt.Columns.Add(new DataColumn("Nota Definitiva"));
            dt.Columns.Add(new DataColumn("Fecha Inscripcion"));
            dt.Columns.Add(new DataColumn("Fecha Inicio"));
            dt.Columns.Add(new DataColumn("Fecha Final"));
            dt.Columns.Add(new DataColumn("Estado"));

            CollectionReference usersRef = db.Collection("Matricula");
            QuerySnapshot snapshot = await usersRef.GetSnapshotAsync();



            foreach (DocumentSnapshot document in snapshot.Documents)
            {
                Console.WriteLine("Materia: {0}", document.Id);
                Dictionary<string, object> documentDictionary = document.ToDictionary();

                dr = dt.NewRow();


                int doc = Convert.ToInt32(documentDictionary["pkEstudiante"].ToString());

                int codmat = Convert.ToInt32(documentDictionary["pkMateria"].ToString());


                CollectionReference estudianteRef = db.Collection("Estudiante");
                Query queryest = estudianteRef.WhereEqualTo("documento", doc);
                QuerySnapshot queryestSnapshot = await queryest.GetSnapshotAsync();

                
                foreach (DocumentSnapshot docest in queryestSnapshot.Documents)
                {
                    Dictionary<string, object> documentDictionaryest = docest.ToDictionary();

                    dr["Estudiante"] = documentDictionaryest["nombres"] + " " + documentDictionaryest["apellidos"];
                }


                CollectionReference materiasRef = db.Collection("Materia");
                Query querymate = materiasRef.WhereEqualTo("codigo", codmat);
                QuerySnapshot querymatSnapshot = await querymate.GetSnapshotAsync();

                foreach (DocumentSnapshot docmat in querymatSnapshot.Documents)
                {
                    Dictionary<string, object> documentDictionarymat = docmat.ToDictionary();
                    dr["Materia"] = documentDictionarymat["nombre"];
                }

                dr["Nota Definitiva"] = documentDictionary["notaDefinitiva"];
                dr["Fecha Inscripcion"] = documentDictionary["fechaInscripcion"];
                dr["Fecha Inicio"] = documentDictionary["fechaInicio"];
                dr["Fecha Final"] = documentDictionary["fechaFinal"];
                int estado = Convert.ToInt32(documentDictionary["estado"]);
               if (estado == 0)
                {
                    dr["Estado"] = "Matriculado";
                }
                else if (estado == 1)
                {
                    dr["Estado"] = "Cursando";
                }
                else if (estado == 2)
                {
                    dr["Estado"] = "Reprobado";
                }
                else if (estado == 3)
                {
                    dr["Estado"] = "Aprobado";
                }



                dt.Rows.Add(dr);
            }

            gridList.DataSource = dt;
            gridList.DataBind();
           

        }


    }
    }
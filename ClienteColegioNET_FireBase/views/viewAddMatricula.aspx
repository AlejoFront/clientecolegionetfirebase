﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="viewAddMatricula.aspx.cs" Inherits="ColegioClienteNET_FireBase.Views.viewAddMatricula" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cliente .NET FireBase | Colegio</title>
    <link rel="stylesheet" href="../Libs/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../libs/css/footer.css"/>
    <link rel="stylesheet" href="../libs/css/styles.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <a class="navbar-brand" href="#">Colegio</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="">Principal</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Matricula</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="viewListMatricula.aspx">Listar | Matricula</a>
                        <a class="dropdown-item" href="viewAddMatricula.aspx">Agregar | Matricula</a>
                        <a class="dropdown-item" href="viewSearchMatricula.aspx">Filtrar Matricula por dos parametros</a>
                        <a class="dropdown-item" href="viewDeleteMatricula.aspx">Buscar, Eliminar | Matricula</a>
                        <a class="dropdown-item" href="viewEditMatricula.aspx">Buscar, Editar | Matricula</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="actualizar()">Actualizar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="acercaDe()">Acerca de ....</a>
                </li>
            </ul>
        </nav>


        <h2 class="title">Agregar MAtricula</h2>

        <div class="cont-bus-add">
            <div class="group-cont">
                <asp:TextBox ID="txtDocumento" runat="server" CssClass="form-control" 
                    placeholder="Documento del Estudiante"></asp:TextBox>
                <asp:Button ID="btnBuscarEst" runat="server" Text="Buscar" CssClass="btn btn-success" OnClick="btnBuscarEst_Click"/>
            </div>
            <div class="group-cont">
                <select id="SelectGrado" class="form-control" runat="server">
                <option value="none"> Selecciona El grado</option>
                <option value="1">Grado 1</option>
                <option value="2">Grado 2</option>
                <option value="3">Grado 3</option>
                <option value="4">Grado 4</option>
                <option value="5">Grado 5</option>
                <option value="6">Grado 6</option>
                <option value="7">Grado 7</option>
                <option value="8">Grado 8</option>
                <option value="9">Grado 9</option>
                <option value="10">Grado 10</option>
                <option value="11">Grado 11</option>

                </select>
                <asp:Button ID="btnBuscarGrado" runat="server" Text="Buscar" CssClass="btn btn-success" OnClick="btnBuscarGrado_Click"/>
            </div>

        </div>
         <div class="cont-tab-add">
             <div class="group-cont-add est">
                 <asp:GridView ID="grillalistest" runat="server"></asp:GridView>
              </div>



             <div class="group-cont-add mater">
                 <asp:GridView ID="grillalistgrado" runat="server"></asp:GridView>


                 <div class="cont-btn mt-3">
                     <div class="form-group">
                         <asp:Label ID="lblfechinicio" runat="server" Text="Fecha Inicio"></asp:Label>
                         <input id="fechainicio" type="date" class="form-control" runat="server"/>
                     </div>
                     <div class="form-group">
                         <asp:Label ID="lblfechfinal" runat="server" Text="Fecha Final"></asp:Label>
                         <input id="fechafin" type="date" class="form-control" runat="server"/>
                     </div>

                     <asp:Button ID="btnMatricular" runat="server" Text="Matricular" class="btn btn-success btnpersonal" OnClick="btnMatricular_Click" Visible="False"/>
                 </div>
             </div>


         </div>



    </form>



         <script src="../libs/js/jquery-3.2.1.slim.min.js"></script>
    <script src="../libs/js/popper.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
    <script src="../libs/js/sweetalert.min.js"></script>
    <script src="../libs/js/fontawesome.js"></script>
    <script src="../libs/js/aplicacion.js"></script>
</body>
</html>

﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClienteColegioNET_FireBase
{
    public partial class test : System.Web.UI.Page
    {
        private  FirestoreDb db = servicio.Connection.getConnection();
        protected async void Page_Load(object sender, EventArgs e)
        {

            DataTable dt = new System.Data.DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("Estudiante"));
            dt.Columns.Add(new DataColumn("Materia"));
            dt.Columns.Add(new DataColumn("Nota Definitiva"));

            CollectionReference usersRef = db.Collection("Matricula");
            QuerySnapshot snapshot = await usersRef.GetSnapshotAsync();

            foreach (DocumentSnapshot document in snapshot.Documents)
            {
                Console.WriteLine("Materia: {0}", document.Id);
                Dictionary<string, object> documentDictionary = document.ToDictionary();

                dr = dt.NewRow();
                dr["Estudiante"] = documentDictionary["pkEstudiante"];
                dr["Materia"] = documentDictionary["pkMateria"];
                dr["Nota Definitiva"] = documentDictionary["notaDefinitiva"];

                dt.Rows.Add(dr);
            }

            gridList.DataSource = dt;
            gridList.DataBind();
        }
    }
}
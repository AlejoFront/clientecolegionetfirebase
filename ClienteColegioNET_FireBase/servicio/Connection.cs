﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClienteColegioNET_FireBase.servicio
{
    public class Connection
    {
        private static FirestoreDb db;

        private Connection()
        {

        }

        public static FirestoreDb getConnection()
        {
            String path = AppDomain.CurrentDomain.BaseDirectory + @"coneccion.json";
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);
            
            if(db == null)
            {
                db = FirestoreDb.Create("proyectosoa2020bcolegio");
            }


            return db;
        }


    }
}